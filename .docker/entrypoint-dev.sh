#!/bin/sh

echo "--- All Unit Tests ---"
cd /app
python -m test

echo "--- Run Forever ---"
echo ""
echo "to test in development:"
echo ""
echo "sudo docker exec -it docker_test_1 /bin/bash"
echo "python -m test [module][.submodule]"
tail -f /dev/null
