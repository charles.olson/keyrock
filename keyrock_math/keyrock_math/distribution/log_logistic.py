import logging
import math
import numpy as np
from typing import Literal

from .analytic import Distribution

logger = logging.getLogger(__name__)

class LogLogistic(Distribution):
    t: Literal['log_logistic'] = 'log_logistic'
    shape: float = 1    # g
    scale: float = 1    # s (50th percentile)

    # mode = s * ((g - 1)/(g + 1))^(1/g) -> 2.2
    # mean = s * (pi / g) / sin(pi / g)

    @property
    def x_min(self) -> float:
        return 0.0

    @property
    def x_max(self) -> float:
        return math.inf

    def cdf(self, x: float) -> float:
        xb = np.power(x, self.shape)
        sb = np.power(self.scale, self.shape)
        return xb / (sb + xb)

    def pdf(self, x: float) -> float:
        xs = x / self.scale
        bs = self.shape / self.scale
        numerator = bs * np.power(xs, self.shape - 1)
        denominator = np.square(np.power(xs, self.shape) + 1)
        return numerator / denominator

    def quantile(self, p: float) -> float:
        return self.scale * np.power(p / (1.0 - p), 1.0 / self.shape)

    def get_random_value(self, size=1) -> float:
        raise NotImplementedError('LogLogistic rng not implemented')
