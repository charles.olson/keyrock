# Keyrock


## Getting Started

```
cd .docker
docker-compose build
docker-compose up
```

in a separate console:
```
docker exec -it keyrock_test_1 /bin/bash
python -m test [module][.submodule]
```

## Adding as a Submodule to another Project

git submodule add [HTTPS URL (for Gitlab-ci reasons)]

In .gitmodules, add:
```
branch = main
```

Replace the URL with SSH in .git/config and .git/modules/modules/[module name]/config; e.g:
```
[submodule "modules/keyrock"]
	url = git@gitlab.com:charles.olson/keyrock.git
	branch = main
	active = true
```
