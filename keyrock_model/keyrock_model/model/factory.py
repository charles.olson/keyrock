from pydantic import TypeAdapter

class Factory():
    def __init__(self, root_type):
        self.root_type = root_type
        self.adapter = TypeAdapter(self.root_type)

    def get_instance(self, config):
        return self.adapter.validate_python(config)
