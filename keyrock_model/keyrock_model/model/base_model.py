from pydantic import BaseModel, ConfigDict

class KrBaseModel(BaseModel):
    model_config = ConfigDict(extra='allow')

    def __init__(self, **kw):
        super().__init__(**kw)
        self.__post_init__()

    def __post_init__(self):
        pass
